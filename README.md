# eslint-config-sheathing

[Shareable ESLint
configuration](https://eslint.org/docs/latest/extend/shareable-configs) for
repos generated using the [Sheathing
cookiecutter](https://gitlab.com/alphafork/react-cladding/cookiecutters/sheathing).

## Usage

The shareable ESLint configuration is defined in the `.eslintrc.js` file of this
repo, In order to use the same, install this package as follows:

`npm install --save-dev gitlab:alphafork/react-cladding/utils/eslint-config-sheathing`

Now create ESLint configuration file named `.eslintrc.json` in the root of your
project repo and add the following to it:

```
{
  "extends": "sheathing"
}
```

That's it, now you can run `eslint path/to/src`.


## License

[GPL-3.0-or-later](LICENSE)


## Contact

[Alpha Fork Technologies](https://alphafork.com)

Email: [connect@alphafork.com](mailto:connect@alphafork.com)
